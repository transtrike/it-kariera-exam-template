using AutoMapper;
using ExamTemplate.Data.Models;
using ExamTemplate.Services.Models;

namespace ExamTemplate.Services.Configurations
{
	public class UserMappings : Profile
	{
		public UserMappings()
		{
			CreateMap<RegisterUserServiceModel, User>();
		}
	}
}
