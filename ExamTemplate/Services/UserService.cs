﻿using System.Threading.Tasks;
using AutoMapper;
using ExamTemplate.Data.Models;
using ExamTemplate.Data.Repositories;
using ExamTemplate.Services.Models;
using Microsoft.AspNetCore.Identity;

namespace ExamTemplate.Services
{
	public class UserService
	{
		private readonly IMapper _autoMapper;
		private readonly UserRepository _userRepository;
		private readonly SignInManager<User> _signInManager;

		public UserService(IMapper autoMapper, UserRepository userRepository, SignInManager<User> signInManager)
		{
			this._autoMapper = autoMapper;
			this._userRepository = userRepository;
			this._signInManager = signInManager;
		}

		public async Task<bool> RegisterUserAsync(RegisterUserServiceModel registerUserServiceModel)
		{
			User user = this._autoMapper.Map<User>(registerUserServiceModel);

			bool userCreateResult = await this._userRepository.AddAsync(user, registerUserServiceModel.Password);
			bool addRoleResult = await this._userRepository.AddRoleToUserAsync(user, Role.UserRole);

			return userCreateResult && addRoleResult;
		}

		public async Task<bool> LoginUserAsync(LoginUserServiceModel loginUserServiceModel)
		{
			User user = await this._userRepository.GetByUsernameAsync(loginUserServiceModel.Username);

			var result = await this._signInManager.PasswordSignInAsync(loginUserServiceModel.Username, loginUserServiceModel.Password, false, false);

			return result.Succeeded;
		}

		public async Task LogoutAsync()
		{
			await this._signInManager.SignOutAsync();
		}
	}
}
