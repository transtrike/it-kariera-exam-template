namespace ExamTemplate.Services.Models
{
	public class LoginUserServiceModel
	{
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
