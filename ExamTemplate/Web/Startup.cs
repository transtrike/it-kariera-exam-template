using System;
using System.Linq;
using ExamTemplate.Data;
using ExamTemplate.Data.Models;
using ExamTemplate.Data.Repositories;
using ExamTemplate.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Web
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			this.Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllersWithViews();
			services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

			/*
			 * Dependency Injection configuration
			 */

			services.AddTransient<UserService>();
			services.AddTransient<UserRepository>();

			/*
			 * Database configuration
			 */
			services.AddDbContext<TemplateContext>(options =>
				options.UseNpgsql(this.Configuration.GetConnectionString("LocalDBConnection")));

			// Needed for SignInManager and UserManager
			services.AddIdentity<User, Role>()
				.AddRoles<Role>()
				.AddEntityFrameworkStores<TemplateContext>();

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthorization();
			app.UseAuthentication();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});

			/*
			 * Make sure that the database is migrated
			 * and that the User and Admin role exist in database
			 */

			using var serviceScope = app.ApplicationServices.CreateScope();
			using var dbContext = serviceScope.ServiceProvider.GetRequiredService<TemplateContext>();

			dbContext.Database.Migrate();

			var roleManager = (RoleManager<Role>)serviceScope.ServiceProvider.GetService(typeof(RoleManager<Role>));
			if (!dbContext.Roles.Any(x => x.Name == Role.UserRole))
			{
				Role userRole = new() { Name = Role.UserRole };
				roleManager.CreateAsync(userRole).Wait();
			}
			if (!dbContext.Roles.Any(x => x.Name == Role.AdminRole))
			{
				Role adminRole = new() { Name = Role.AdminRole };
				roleManager.CreateAsync(adminRole).Wait();
			}

		}
	}
}
