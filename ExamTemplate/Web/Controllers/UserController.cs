using ExamTemplate.Services;
using Microsoft.AspNetCore.Mvc;
using ExamTemplate.Web.Models.User;
using AutoMapper;
using ExamTemplate.Services.Models;
using System.Threading.Tasks;

namespace ExamTemplate.Web.Controllers
{
	public class UserController : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly UserService _userService;

		public UserController(IMapper autoMapper, UserService userService)
		{
			this._autoMapper = autoMapper;
			this._userService = userService;
		}

		[HttpGet]
		[Route("/Register")]
		public IActionResult Register()
		{
			return View();
		}

		[HttpPost]
		[Route("/Register")]
		public async Task<IActionResult> Register(RegisterUserViewModel registerUserViewModel)
		{
			RegisterUserServiceModel registerUserServiceModel = this._autoMapper.Map<RegisterUserServiceModel>(registerUserViewModel);

			bool result = await this._userService.RegisterUserAsync(registerUserServiceModel);

			if (result)
				return RedirectToAction("Index", "Home");
			else
				return View();
		}

		[HttpGet]
		[Route("/Login")]
		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[Route("/Login")]
		public async Task<IActionResult> Login(LoginUserViewModel loginUserViewModel)
		{
			LoginUserServiceModel loginUserServiceModel = this._autoMapper.Map<LoginUserServiceModel>(loginUserViewModel);

			bool result = await this._userService.LoginUserAsync(loginUserServiceModel);

			if (result)
				return RedirectToAction("Index", "Home");
			else
				return View();
		}

		[HttpPost]
		public async Task<IActionResult> Logout()
		{
			await this._userService.LogoutAsync();

			return RedirectToAction("Login");
		}
	}
}
