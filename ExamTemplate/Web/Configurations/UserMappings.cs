using AutoMapper;
using ExamTemplate.Services.Models;
using ExamTemplate.Web.Models.User;

namespace ExamTemplate.Services.Configurations
{
	public class UserMappings : Profile
	{
		public UserMappings()
		{
			CreateMap<RegisterUserViewModel, RegisterUserServiceModel>();
			CreateMap<LoginUserViewModel, LoginUserServiceModel>();
		}
	}
}
