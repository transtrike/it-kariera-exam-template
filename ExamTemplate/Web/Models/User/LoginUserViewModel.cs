namespace ExamTemplate.Web.Models.User
{
	public class LoginUserViewModel
	{
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
