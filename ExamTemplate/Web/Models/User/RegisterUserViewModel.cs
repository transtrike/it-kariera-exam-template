namespace ExamTemplate.Web.Models.User
{
	public class RegisterUserViewModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
