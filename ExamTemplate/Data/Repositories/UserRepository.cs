using System.Threading.Tasks;
using ExamTemplate.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ExamTemplate.Data.Repositories
{
	public class UserRepository
	{
		private readonly TemplateContext _context;
		private readonly UserManager<User> _userManager;
		private readonly RoleManager<Role> _roleManager;

		public UserRepository(TemplateContext templateContext, UserManager<User> userManager, RoleManager<Role> roleManager)
		{
			this._context = templateContext;
			this._userManager = userManager;
			this._roleManager = roleManager;
		}

		public async Task<User> GetByUsernameAsync(string username)
		{
			return await this._userManager.Users
				.Include(x => x.Roles)
				.FirstOrDefaultAsync(x => x.UserName == username);
		}

		public async Task<bool> AddAsync(User user, string password)
		{
			user.PasswordHash = this._userManager.PasswordHasher.HashPassword(user, password);
			IdentityResult result = await this._userManager.CreateAsync(user);

			return result.Succeeded;
		}

		public async Task<bool> AddRoleToUserAsync(User user, string roleName)
		{
			bool succeeded = (await this._userManager.AddToRoleAsync(user, roleName)).Succeeded;
			if (succeeded)
			{
				user.Roles.Add(await this._roleManager.FindByNameAsync(roleName));
				succeeded = await this._context.SaveChangesAsync() >= 1;
			}

			return succeeded;
		}

		public async Task<bool> VerifyPasswordAsync(User user, string password)
		{
			return await this._userManager.CheckPasswordAsync(user, password);
		}
	}
}
