﻿using System;
using ExamTemplate.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ExamTemplate.Data
{
	public class TemplateContext : IdentityDbContext<User, Role, Guid>
	{
		public TemplateContext(DbContextOptions<TemplateContext> options)
			: base(options) { }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			/* Custom logic here! */

			base.OnModelCreating(builder);
		}
	}
}
