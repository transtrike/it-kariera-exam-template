using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamTemplate.Data.Models
{
	[Table("Roles")]
	public class Role : IdentityRole<Guid>
	{
		public static string UserRole = "User";
		public static string AdminRole = "Administrator";

		public List<User> Users { get; set; } = new List<User>();
	}
}
