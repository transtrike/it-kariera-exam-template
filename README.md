# IT-kariera Exam Template

A template that could be useful for the final IT-kariera exam in 2021

## Notes

There are some configs that I've made that you may want to change. This section explains them.

### Migrations

The project requires Migrations to be made by hand. Make sure you have configured a connection string!!!

Shell (dotnet-ef tool installed):
```bash
cd ExamTemplate/Data
dotnet ef migrations add InitialMigrations
dotnet ef database update
```

Visual Studio (Install package `Microsoft.EntityFrameworkCore.Tools`):
```powershell
cd ExamTemplate/Data
Add-Migration InitialCreate
Update-Database
```

### Database

The project is made to use [PostgreSQL](https://www.postgresql.org/) by default. Since not everyone would want to use that, here are some steps for other databases.

Using MySQL:
1. Install the [MySql.Data.EntityFrameworkCore](https://www.nuget.org/packages/MySql.Data.EntityFrameworkCore/) NuGet package in **both** `ExamTemplate/Web` and `ExamTemplate/Data`
2. In the `ConfigureServices` method, inside `ExamTemplate/Web/Startup.cs`, **and** in the `CreateDbContext` method, inside `ExamTemplate/Data/TemplateContextFactory.cs` change `UseNpgsql` to `UseMySQL`
   - You'll also need to add a using for the package in step 1

Using SQL Server (MSSQL):
1. Install the [Microsoft.EntityFrameworkCore.SqlServer](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.SqlServer/) NuGet package in **both** `ExamTemplate/Web` and `ExamTemplate/Data`
2. In the `ConfigureServices` method, inside `ExamTemplate/Web/Startup.cs`, **and** in the `CreateDbContext` method, inside `ExamTemplate/Data/TemplateContextFactory.cs` change `UseNpgsql` to `UseSqlServer`
   - You'll also need to add a using for the package in step 1


### HTTPS

HTTPS is disabled by default, since certificates could cause issues on some systems.

To revert:
1. Add `app.UseHttpsRedirection();` in the `Configure` method, inside `ExamTemplate/Web/Startup.cs`
2. Add `https://localhost:5001;` in `"applicationUrl"` in the `Web` section, inside `ExamTemplate/Web/Properties/launchSettings.json`.
   - So, line 21 in the `launchSettings.json` file should look like: `"applicationUrl": "https://localhost:5001;http://localhost:5000",`
